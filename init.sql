CREATE DATABASE IF NOT EXISTS simplonline_db CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'simplon'@'localhost' IDENTIFIED WITH mysql_native_password BY 'simplon';
GRANT ALL ON simplonline_db.* TO 'simplon'@'localhost';
FLUSH PRIVILEGES;