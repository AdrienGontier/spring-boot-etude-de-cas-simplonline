package com.simplonline.EDCsimplonline.entities;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "tagId"
)
public class Tag {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String tagId;

    private String title;

    private String category;

    @ManyToMany
    @JoinTable(
            name = "brief_tag",
            joinColumns = @JoinColumn(name = "tagId"),
            inverseJoinColumns = @JoinColumn(name = "briefId"))
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<Brief> brief ;

    public Tag(Collection<Brief> brief) {
        this.brief = brief;
    }

    public Tag() {
    }

    public Tag(String tagId, String title, String category, Collection<Brief> brief) {
        this.tagId = tagId;
        this.title = title;
        this.category = category;
        this.brief = brief;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Collection<Brief> getBrief() {
        return brief;
    }

    public void setBrief(Collection<Brief> brief) {
        this.brief = brief;
    }
}
