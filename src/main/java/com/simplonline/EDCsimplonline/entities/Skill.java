package com.simplonline.EDCsimplonline.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.simplonline.EDCsimplonline.enums.Level;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Skill {

    @Id
    @Column(nullable = false)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String skillId;

    private String title;

    @Enumerated(EnumType.STRING)
    private Level level;

    @ManyToMany
    @JoinTable(
            name = "skill_user",
            joinColumns = @JoinColumn(name = "skillId"),
            inverseJoinColumns = @JoinColumn(name = "userId")
    )
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<User> users;

    @ManyToMany
    @JoinTable(
            name = "brief_skill",
            joinColumns = @JoinColumn(name = "skillId"),
            inverseJoinColumns = @JoinColumn(name = "briefId")
    )
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<Brief> briefs;

    @ManyToMany
    @JoinTable(
            name = "skill_render",
            joinColumns = @JoinColumn(name = "skillId"),
            inverseJoinColumns = @JoinColumn(name = "renderId")
    )
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<Render> renders;

   @ManyToOne
   @JoinColumn(name = "id")
   @JsonProperty(access = JsonProperty.Access.READ_ONLY)
   private Reference reference;

    public Skill() {
    }

    public Skill(String skillId, String title, Level level, Collection<User> users, Collection<Brief> briefs, Collection<Render> renders, Reference reference) {
        this.skillId = skillId;
        this.title = title;
        this.level = level;
        this.users = users;
        this.briefs = briefs;
        this.renders = renders;
        this.reference = reference;
    }

    public String getSkillId() {
        return skillId;
    }

    public void setSkillId(String idSkill) {
        this.skillId = idSkill;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }

    public Collection<Brief> getBriefs() {
        return briefs;
    }

    public void setBriefs(Collection<Brief> briefs) {
        this.briefs = briefs;
    }

    public Collection<Render> getRenders() {
        return renders;
    }

    public void setRenders(Collection<Render> renders) {
        this.renders = renders;
    }

    public Reference getReference() {
        return reference;
    }

    public void setReference(Reference reference) {
        this.reference = reference;
    }
}
