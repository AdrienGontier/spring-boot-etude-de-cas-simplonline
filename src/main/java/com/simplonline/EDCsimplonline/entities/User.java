package com.simplonline.EDCsimplonline.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "userId"
)
public class User {

    @Id
    @Column(nullable = false)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String userId;

    private String email;

    private String password;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
        name = "user_role",
        joinColumns = @JoinColumn(name = "userId"),
        inverseJoinColumns = @JoinColumn(name = "roleId"))
    private Collection<Role> roles ;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "users_renders",
        joinColumns = @JoinColumn(name = "userId"),
        inverseJoinColumns = @JoinColumn(name = "renderId"))
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<Render> renders;

    @OneToMany
    @JoinTable(
        name = "brief_user",
        joinColumns = @JoinColumn(name = "userId"),
        inverseJoinColumns = @JoinColumn(name = "briefId"))
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<Brief> briefs;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<Brief> briefAuthors;

    @ManyToMany
    @JoinTable(
        name = "skill_user",
        joinColumns = @JoinColumn(name = "userId"),
        inverseJoinColumns = @JoinColumn(name = "skillId")
    )
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<Skill> skills;

    public User() {
    }

    public User(String userId, String email, String password, Collection<Role> roles, Collection<Render> renders, Collection<Brief> briefs, Collection<Brief> briefAuthors, Collection<Skill> skills) {
        this.userId = userId;
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.renders = renders;
        this.briefs = briefs;
        this.briefAuthors = briefAuthors;
        this.skills = skills;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public Collection<Render> getRenders() {
        return renders;
    }

    public void setRenders(Collection<Render> renders) {
        this.renders = renders;
    }

    public Collection<Brief> getBriefs() {
        return briefs;
    }

    public void setBriefs(Collection<Brief> briefs) {
        this.briefs = briefs;
    }

    public Collection<Brief> getBriefAuthors() {
        return briefAuthors;
    }

    public void setBriefAuthors(Collection<Brief> briefAuthors) {
        this.briefAuthors = briefAuthors;
    }

    public Collection<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Collection<Skill> skills) {
        this.skills = skills;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                ", renders=" + renders +
                ", briefs=" + briefs +
                ", briefAuthors=" + briefAuthors +
                ", skills=" + skills +
                '}';
    }
}
