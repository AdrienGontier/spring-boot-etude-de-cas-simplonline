package com.simplonline.EDCsimplonline.entities;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.simplonline.EDCsimplonline.enums.BriefNotification;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "briefId"
)
public class Brief {


    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String briefId;

    private String title;

    private Date date;

    public String description;

    private String content;

    private String deliverable;

    @Enumerated(EnumType.STRING)
    public BriefNotification notification ;

    public String pedagogicalModality;

    @ManyToMany
    @JoinTable(
            name = "brief_tag",
            joinColumns = @JoinColumn(name = "briefId"),
            inverseJoinColumns = @JoinColumn(name = "tagId"))
    private Collection<Tag> tags ;

    @ManyToMany
    @JoinTable(
            name = "brief_user",
            joinColumns = @JoinColumn(name = "briefId"),
            inverseJoinColumns = @JoinColumn(name = "userId"))
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<User> users;

    @OneToOne(mappedBy = "brief")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Render render ;

    @ManyToMany
    @JoinTable(
            name = "brief_resource",
            joinColumns = @JoinColumn(name = "briefId"),
            inverseJoinColumns = @JoinColumn(name = "resourceId"))
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<Resource> resources;

    @ManyToMany
    @JoinTable(
            name = "brief_skill",
            joinColumns = @JoinColumn(name = "briefId"),
            inverseJoinColumns = @JoinColumn(name = "skillId"))
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<Skill> skills;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
    private User author;

    public Brief() {
    }

    public Brief(String briefId, String title, Date date, String description, String content, String deliverable, BriefNotification notification, String pedagogicalModality, Collection<Tag> tags, Collection<User> users, Render render, Collection<Resource> resources, Collection<Skill> skills, User author) {
        this.briefId = briefId;
        this.title = title;
        this.date = date;
        this.description = description;
        this.content = content;
        this.deliverable = deliverable;
        this.notification = notification;
        this.pedagogicalModality = pedagogicalModality;
        this.tags = tags;
        this.users = users;
        this.render = render;
        this.resources = resources;
        this.skills = skills;
        this.author = author;
    }

    public void setBriefId(String briefId) {
        this.briefId = briefId;
    }

    public String getBriefId() {
        return briefId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDeliverable() {
        return deliverable;
    }

    public void setDeliverable(String deliverable) {
        this.deliverable = deliverable;
    }
    
    public BriefNotification getNotification() {
        return notification;
    }

    public void setNotification(BriefNotification notification) {
        this.notification = notification;
    }

    public String getPedagogicalModality() {
        return pedagogicalModality;
    }

    public void setPedagogicalModality(String pedagogicalModality) {
        this.pedagogicalModality = pedagogicalModality;
    }

    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Collection<Tag> getTags() {
        return tags;
    }

    public void setTags(Collection<Tag> tags) {
        this.tags = tags;
    }

   public Render getRender() {
        return render;
    }

    public void setRenders(Render render) {
        this.render = render;
    }

    public Collection<Resource> getResources() {
        return resources;
    }

    public void setResources(Collection<Resource> resources) {
        this.resources = resources;
    }

    public Collection<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Collection<Skill> skills) {
        this.skills = skills;
    }
}
