package com.simplonline.EDCsimplonline.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.simplonline.EDCsimplonline.enums.RenderType;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Entity
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "renderId"
)
public class Render {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String renderId;

    @Enumerated(EnumType.STRING)
    private RenderType type;

    private String message;

    @OneToMany(mappedBy="render", cascade = CascadeType.ALL)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Collection<Attachment> attachments ;

    @OneToMany(mappedBy="render", cascade = CascadeType.MERGE)
    private Collection<Link> links ;

    @OneToOne
    @JoinColumn(name="briefId")
    private Brief brief;

    @ManyToOne
    @JoinColumn(name="userId")
    private User user ;

    public Render() {
    }

    public Render(String renderId, RenderType type, String message, Collection<Attachment> attachments, Collection<Link> links, Brief brief, User user) {
        this.renderId = renderId;
        this.type = type;
        this.message = message;
        this.attachments = attachments;
        this.links = links;
        this.brief = brief;
        this.user = user;
    }

    public String getRenderId() {
        return renderId;
    }

    public void setRenderId(String renderId) {
        this.renderId = renderId;
    }

    public RenderType getType() {
        return type;
    }

    public void setType(RenderType type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Collection<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Collection<Attachment> attachments) {
        this.attachments = attachments;
    }

    public Collection<Link> getLinks() {
        return links;
    }

    public void setLinks(Collection<Link> links) {
        this.links = links;
    }

    public Brief getBrief() {
        return brief;
    }

    public void setBrief(Brief brief) {
        this.brief = brief;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Render{" +
                "renderId='" + renderId + '\'' +
                ", type=" + type +
                ", message='" + message + '\'' +
                ", attachments=" + attachments +
                ", brief='" + brief + '\'' +
                ", user=" + user +
                '}';
    }
}
