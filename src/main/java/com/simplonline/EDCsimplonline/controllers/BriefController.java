package com.simplonline.EDCsimplonline.controllers;


import com.simplonline.EDCsimplonline.entities.Brief;
import com.simplonline.EDCsimplonline.services.BriefService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/briefs")
public class BriefController {

    private final BriefService briefService;

    public BriefController(BriefService briefService) {
        this.briefService = briefService;
    }

    @PostMapping()
    public ResponseEntity<Brief> createBrief(@RequestBody Brief briefRequest) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(briefService.createBrief(briefRequest));
    }

    @GetMapping("/{briefId}")
    public ResponseEntity<Brief> getBriefById(@PathVariable String briefId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(briefService.getBriefById(briefId));
    }

    @PutMapping("/{briefId}")
    public ResponseEntity<Brief> updateBrief(@PathVariable String briefId, @RequestBody Brief briefRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(briefService.updateBrief(briefId, briefRequest));
    }

    @DeleteMapping("/{briefId}")
    public ResponseEntity<String> deleteBrief(@PathVariable String briefId) throws URISyntaxException {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(briefService.deleteBrief(briefId));
    }

    @GetMapping()
    public ResponseEntity<Page<Brief>> getAllBriefs(
            @RequestParam( name = "page", defaultValue = "0" ) int page,
            @RequestParam( name = "size", defaultValue = "10" ) int size) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(briefService.getAllBriefs(page, size));
    }

}
