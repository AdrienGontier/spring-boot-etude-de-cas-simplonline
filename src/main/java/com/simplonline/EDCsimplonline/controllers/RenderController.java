package com.simplonline.EDCsimplonline.controllers;

import com.simplonline.EDCsimplonline.entities.Render;
import com.simplonline.EDCsimplonline.services.RenderService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/renders")
public class RenderController {
    private final RenderService renderService;

    public RenderController(RenderService renderService) {
        this.renderService = renderService;
    }

    @PostMapping()
    public ResponseEntity<Render> createRender(@RequestPart("data") Render renderRequest, @RequestPart("files") MultipartFile[] files) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(renderService.createRender(renderRequest, files));
    }

    @GetMapping("/{renderId}")
    public ResponseEntity<Render> getRenderById(@PathVariable String renderId) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(renderService.getRenderById(renderId));
    }

    @PutMapping("/{renderId}")
    public ResponseEntity<Render> updateRender(@PathVariable String renderId, @RequestBody Render render) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(renderService.updateRender(renderId, render));
    }

    @DeleteMapping("/{renderId}")
    public ResponseEntity<String> deleteRender(@PathVariable String renderId) throws URISyntaxException {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(renderService.deleteRender(renderId));
    }

    @GetMapping()
    public ResponseEntity<Page<Render>> getAllRenders(
            @RequestParam( name = "page", defaultValue = "0" ) int page,
            @RequestParam( name = "size", defaultValue = "10" ) int size) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(renderService.getAllRenders(page,size));
    }
}
