package com.simplonline.EDCsimplonline.controllers;

import com.simplonline.EDCsimplonline.entities.Role;
import com.simplonline.EDCsimplonline.services.RoleService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/roles")
public class RoleController {
    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PostMapping()
    public ResponseEntity<Role> createRole(@RequestBody Role roleRequest) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(roleService.createRole(roleRequest));
    }

    @GetMapping("/{roleId}")
    public ResponseEntity<Role> getRoleById(@PathVariable String roleId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(roleService.getRoleById(roleId));
    }

    @PutMapping("/{roleId}")
    public ResponseEntity<Role> updateRole(@PathVariable String roleId, @RequestBody Role roleRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(roleService.updateRole(roleId, roleRequest));
    }

    @DeleteMapping("/{roleId}")
    public ResponseEntity<String> deleteRole(@PathVariable String roleId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(roleService.deleteRole(roleId));
    }

    @GetMapping()
    public ResponseEntity<Page<Role>> getAllRoles(
            @RequestParam( name = "page", defaultValue = "0" ) int page,
            @RequestParam( name = "size", defaultValue = "10" ) int size) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(roleService.getAllRoles(page, size));
    }
}
