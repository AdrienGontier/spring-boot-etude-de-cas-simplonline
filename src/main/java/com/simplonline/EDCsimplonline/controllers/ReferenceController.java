package com.simplonline.EDCsimplonline.controllers;

import com.simplonline.EDCsimplonline.entities.Reference;
import com.simplonline.EDCsimplonline.services.ReferenceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api/references")
public class ReferenceController {
    private final ReferenceService referenceService;

    ReferenceController(ReferenceService referenceService) {
        this.referenceService = referenceService;
    }

    @GetMapping()
    public ResponseEntity<Collection<Reference>> getAll() {
        return new ResponseEntity<>(referenceService.getAll(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Reference> getById(@PathVariable String id) {
        return new ResponseEntity<>(referenceService.findById(id), HttpStatus.ACCEPTED);
    }

    @PostMapping()
    public ResponseEntity<Reference> create(@RequestBody Reference reference) {
        return new ResponseEntity<>(referenceService.create(reference), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Reference> update(@RequestBody Reference reference,@PathVariable String id) {
        return new ResponseEntity<>(referenceService.update(id, reference), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable String id) {
        return new ResponseEntity<>(referenceService.delete(id), HttpStatus.ACCEPTED);
    }
}
