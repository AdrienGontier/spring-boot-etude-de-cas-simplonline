package com.simplonline.EDCsimplonline.controllers;

import com.simplonline.EDCsimplonline.entities.Attachment;
import com.simplonline.EDCsimplonline.services.AttachmentService;
import com.simplonline.EDCsimplonline.services.UploadFileService;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping("/api/attachments")
public class AttachmentController {
    private final AttachmentService attachmentService;
    private final UploadFileService uploadFileService;

    public AttachmentController(AttachmentService attachmentService, UploadFileService uploadFileService) {
        this.attachmentService = attachmentService;
        this.uploadFileService = uploadFileService;
    }

    @GetMapping("/get-attachment-by-id/{attachmentId}")
    public ResponseEntity<Attachment> getAttachmentById(@PathVariable String attachmentId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(attachmentService.getAttachmentById(attachmentId));
    }

    @DeleteMapping
    public ResponseEntity<String> deleteAttachment(
            @RequestParam(name = "attachmentId") String attachmentId) {
        attachmentService.deleteAttachment(attachmentId);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body("Attachment supprimé");
    }

    @GetMapping()
    public ResponseEntity<Page<Attachment>> getAllAttachments(
            @RequestParam( name = "page", defaultValue = "0" ) int page,
            @RequestParam( name = "size", defaultValue = "10" ) int size) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(attachmentService.getAllAttachments(page, size));
    }

    @GetMapping("/{fileName:.+}")
    public ResponseEntity <Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request ) {
        // Load file as Resource
        Resource resource = uploadFileService.loadFileAsResource( fileName );

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType( resource.getFile().getAbsolutePath() );
        } catch ( IOException ex ) {
            //log.info( "Could not determine file type." );
        }

        // Fallback to the default content type if type could not be determined
        if ( contentType == null ) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType( contentType ) )
                .header( HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"" )
                .body( resource );
    }
}
