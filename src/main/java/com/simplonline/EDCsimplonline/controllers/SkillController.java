package com.simplonline.EDCsimplonline.controllers;

import com.simplonline.EDCsimplonline.entities.Skill;
import com.simplonline.EDCsimplonline.services.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/skills")
public class SkillController {

    private final SkillService skillService;

    @Autowired
    public SkillController(SkillService skillService) {
        this.skillService = skillService;
    }

    @GetMapping("/{skillId}")
    public ResponseEntity<Skill> getSkillById(@PathVariable("skillId") String skillId){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(skillService.getSkillById(skillId));
    }

    @GetMapping()
    public ResponseEntity<Page<Skill>> getAllSkills(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(skillService.getAllSkills(page, size));
    }

    @PostMapping()
    public ResponseEntity<Skill> createSkill(@RequestBody Skill skillRequest){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(skillService.createSkill(skillRequest));
    }

    @PutMapping("/{skillId}")
    public ResponseEntity<Skill> updateSkill(@PathVariable String skillId, @RequestBody Skill skillRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(skillService.updateSkill(skillId, skillRequest));
    }

    @DeleteMapping("/{skillId}")
    public ResponseEntity<String> deleteSkill(@PathVariable String skillId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(skillService.deleteSkill(skillId));
    }
}
