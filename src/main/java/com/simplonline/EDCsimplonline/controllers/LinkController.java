package com.simplonline.EDCsimplonline.controllers;

import com.simplonline.EDCsimplonline.entities.Link;
import com.simplonline.EDCsimplonline.services.LinkService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/links")
public class LinkController {
    private final LinkService linkService;

    public LinkController(LinkService linkService) {
        this.linkService = linkService;
    }

    @GetMapping("/{linkId}")
    public ResponseEntity<Link> getLinkById(@PathVariable String linkId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(linkService.getLinkById(linkId));
    }

    @PutMapping("/{linkId}")
    public ResponseEntity<Link> updateLink(@PathVariable String linkId, @RequestBody Link linkRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(linkService.updateLink(linkId, linkRequest));
    }

    @DeleteMapping("/{linkId}")
    public ResponseEntity<String> deleteLink(@PathVariable String linkId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(linkService.deleteLink(linkId));
    }

    @DeleteMapping()
    public ResponseEntity<Page<Link>> getAllLinks(
            @RequestParam( name = "page", defaultValue = "0" ) int page,
            @RequestParam( name = "size", defaultValue = "10" ) int size) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(linkService.getAllLinks(page, size));
    }
}
