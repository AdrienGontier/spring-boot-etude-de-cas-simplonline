package com.simplonline.EDCsimplonline.controllers;

import com.simplonline.EDCsimplonline.entities.Reference;
import com.simplonline.EDCsimplonline.entities.Resource;
import com.simplonline.EDCsimplonline.services.ResourceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api/resources")
public class ResourceController {
    private final ResourceService resourceService;

    public ResourceController(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @GetMapping()
    public ResponseEntity<Collection<Resource>> getAll() {
        return new ResponseEntity<>(resourceService.getAll(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Resource> getById(@PathVariable String id) {
        return new ResponseEntity<>(resourceService.findById(id), HttpStatus.ACCEPTED);
    }

    @PostMapping()
    public ResponseEntity<Resource> create(@RequestBody Resource resource) {
        return new ResponseEntity<>(resourceService.create(resource), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Resource> update(@RequestBody Resource resource, @PathVariable String id) {
        return new ResponseEntity<>(resourceService.update(id, resource), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable String id) {
        return new ResponseEntity<>(resourceService.delete(id), HttpStatus.ACCEPTED);
    }
}
