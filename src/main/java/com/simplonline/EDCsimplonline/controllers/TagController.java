package com.simplonline.EDCsimplonline.controllers;


import com.simplonline.EDCsimplonline.entities.Tag;
import com.simplonline.EDCsimplonline.services.TagService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tags")
public class TagController {

    private final TagService tagService;

    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @PostMapping()
    public ResponseEntity<Tag> createTag(@RequestBody Tag tagRequest) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(tagService.createTag(tagRequest));
    }

    @GetMapping("/{tagId}")
    public ResponseEntity<Tag> getTagById(@PathVariable String tagId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(tagService.getTagById(tagId));
    }

    @PutMapping("/{tagId}")
    public ResponseEntity<Tag> updateTag(@PathVariable String tagId, @RequestBody Tag tagRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(tagService.updateTag(tagId, tagRequest));
    }

    @DeleteMapping("/{tagId}")
    public ResponseEntity<String> deleteTag(@PathVariable String tagId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(tagService.deleteTag(tagId));
    }

    @GetMapping()
    public ResponseEntity<Page<Tag>> getAllTags(
            @RequestParam( name = "page", defaultValue = "0" ) int page,
            @RequestParam( name = "size", defaultValue = "10" ) int size) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(tagService.getAllTags(page, size));
    }
}
