package com.simplonline.EDCsimplonline.enums;

public enum RenderType {
    GROUP, INDIVIDUAL
}
