package com.simplonline.EDCsimplonline.enums;

public enum BriefNotification {
    VIEW, DELETED, VIZUALIZED, NOT_VIZUALIZED
}
