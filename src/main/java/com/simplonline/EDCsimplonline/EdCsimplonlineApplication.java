package com.simplonline.EDCsimplonline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdCsimplonlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdCsimplonlineApplication.class, args);
	}

}
