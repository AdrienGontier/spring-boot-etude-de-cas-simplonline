package com.simplonline.EDCsimplonline.services;


import com.simplonline.EDCsimplonline.entities.Tag;
import org.springframework.data.domain.Page;

public interface TagService {

    Tag createTag(Tag tagRequest);

    Tag getTagById(String tagId);

    Tag updateTag(String tagId, Tag tagRequest);

    String deleteTag(String tagId);

    Page<Tag> getAllTags(int page, int size);
}
