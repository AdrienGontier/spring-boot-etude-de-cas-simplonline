package com.simplonline.EDCsimplonline.services;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface UploadFileService {
    String  storeFile( MultipartFile file );
    Resource loadFileAsResource(String fileName );
    String deleteFile(String fileName);
}
