package com.simplonline.EDCsimplonline.services;

import com.simplonline.EDCsimplonline.entities.Render;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.net.URISyntaxException;

public interface RenderService {
    Render createRender(Render renderRequest, MultipartFile[] files);

    Render getRenderById(String renderId);

    Render updateRender(String renderId, Render render);

    String deleteRender(String renderId) throws URISyntaxException;

    Page<Render> getAllRenders(int page, int size);
}
