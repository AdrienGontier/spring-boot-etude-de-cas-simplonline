package com.simplonline.EDCsimplonline.services;

import com.simplonline.EDCsimplonline.entities.Skill;
import com.simplonline.EDCsimplonline.entities.User;
import org.springframework.data.domain.Page;

public interface SkillService {

    Page<Skill> getAllSkills(int page, int size);

    Skill getSkillById(String skillId);

    Skill createSkill(Skill skillRequest);

    Skill updateSkill(String skillId, Skill skillRequest);

    String deleteSkill(String skillId);
}
