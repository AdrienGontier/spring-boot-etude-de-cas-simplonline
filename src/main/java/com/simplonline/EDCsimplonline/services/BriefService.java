package com.simplonline.EDCsimplonline.services;


import com.simplonline.EDCsimplonline.entities.Brief;
import org.springframework.data.domain.Page;

import java.net.URISyntaxException;

public interface BriefService {

    Brief createBrief(Brief briefRequest);

    Brief getBriefById(String briefId);

    Brief updateBrief(String briefId, Brief briefRequest);

    String deleteBrief(String briefId) throws URISyntaxException;

    Page<Brief> getAllBriefs(int page, int size);
}
