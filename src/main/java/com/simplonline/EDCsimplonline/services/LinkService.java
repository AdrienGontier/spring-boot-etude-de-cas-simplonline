package com.simplonline.EDCsimplonline.services;

import com.simplonline.EDCsimplonline.entities.Link;
import org.springframework.data.domain.Page;

public interface LinkService {
    Link createLink(Link linkRequest);

    Link getLinkById(String linkId);

    Link updateLink(String linkId, Link linkRequest);

    String deleteLink(String linkId);

    Page<Link> getAllLinks(int page, int size);
}
