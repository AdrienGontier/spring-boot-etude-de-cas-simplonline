package com.simplonline.EDCsimplonline.services;

import com.simplonline.EDCsimplonline.entities.Reference;

import java.util.Collection;

public interface ReferenceService {

    Collection<Reference> getAll();

    Reference findById(String id);

    Reference create(Reference reference);

    Reference update(String id, Reference reference);

    boolean delete(String id);

}
