package com.simplonline.EDCsimplonline.services.servicesImpl;

import com.simplonline.EDCsimplonline.configurations.properties.FileStorageProperties;
import com.simplonline.EDCsimplonline.exceptions.FileNotFoundException;
import com.simplonline.EDCsimplonline.exceptions.FileStorageException;
import com.simplonline.EDCsimplonline.services.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class UploadFileServiceImpl implements UploadFileService {

    private final Path fileStorageLocation;

    @Autowired
    public UploadFileServiceImpl( FileStorageProperties fileStorageProperties ) {
        this.fileStorageLocation = Paths.get( fileStorageProperties.getUploadDir() )
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories( this.fileStorageLocation );
        } catch ( Exception ex ) {
            throw new FileStorageException( "Could not create the directory where the uploaded files will be stored.", ex );
        }
    }

    @Override
    public String storeFile( MultipartFile file ) {
        String fileName = StringUtils.cleanPath( file.getOriginalFilename() );
        try {
            // Check if the file's name contains invalid characters
            if ( fileName.contains("..") ) {
                throw new FileStorageException( "Sorry! Filename contains invalid path sequence " + fileName );
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve( fileName );
            Files.copy( file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING );
            return ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path( "/api/attachments/")
                    .path( fileName )
                    .toUriString();

        } catch ( IOException ex ) {
            throw new FileStorageException( "Could not store file " + fileName + ". Please try again!", ex );
        }
    }

    @Override
    public Resource loadFileAsResource(String fileName ) {
        try {
            Path filePath = this.fileStorageLocation.resolve( fileName ).normalize();
            Resource resource = new UrlResource( filePath.toUri() );
            if ( resource.exists() ) {
                return resource;
            } else {
                throw new FileNotFoundException( "File not found " + fileName );
            }
        } catch ( MalformedURLException ex ) {
            throw new FileNotFoundException( "File not found " + fileName, ex );
        }
    }

    @Override
    public String deleteFile(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve( fileName ).normalize();
            boolean result = Files.deleteIfExists(filePath);
            if ( result ) {
                return "La pièce jointe a bien été supprimé.";
            } else {
                throw new FileNotFoundException( "File not found " + fileName );
            }
        } catch ( MalformedURLException ex ) {
            throw new FileNotFoundException( "File not found " + fileName, ex );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
