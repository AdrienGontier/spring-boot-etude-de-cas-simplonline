package com.simplonline.EDCsimplonline.services.servicesImpl;

import com.simplonline.EDCsimplonline.entities.Tag;
import com.simplonline.EDCsimplonline.exceptions.ResourceNotFoundException;
import com.simplonline.EDCsimplonline.repositories.TagRepository;
import com.simplonline.EDCsimplonline.services.TagService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }


    @Override
    public Tag createTag(Tag tagRequest) {
        return tagRepository.save(tagRequest);
    }

    @Override
    public Tag getTagById(String tagId) {
        return tagRepository.findById(tagId).orElseThrow(()->
                new ResourceNotFoundException("Ce tag n'existe pas.")
        );
    }

    @Override
    public Tag updateTag(String tagId, Tag tagRequest) {
        var tag = getTagById(tagId);
        tag.setTitle(tagRequest.getTitle());
        tag.setCategory(tagRequest.getCategory());
        return tagRepository.save(tag);
    }

    @Override
    public String deleteTag(String tagId) {
        var tag = getTagById(tagId);
        tagRepository.delete(tag);
        return "Le tag a bien été supprimé";
    }

    @Override
    public Page<Tag> getAllTags(int page, int size) {
        return tagRepository.findAll(PageRequest.of(page, size));
    }
}
