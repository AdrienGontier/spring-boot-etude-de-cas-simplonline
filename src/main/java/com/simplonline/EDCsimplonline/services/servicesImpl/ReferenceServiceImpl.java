package com.simplonline.EDCsimplonline.services.servicesImpl;

import com.simplonline.EDCsimplonline.entities.Reference;
import com.simplonline.EDCsimplonline.repositories.ReferenceRepo;
import com.simplonline.EDCsimplonline.services.ReferenceService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class ReferenceServiceImpl  implements ReferenceService {

    private final ReferenceRepo referenceRepo;

    ReferenceServiceImpl(ReferenceRepo referenceRepo) {
        this.referenceRepo = referenceRepo;
    }

    @Override
    public Collection<Reference> getAll() {
        return referenceRepo.findAll();
    }

    @Override
    public Reference findById(String id) {
        return referenceRepo.findById(id);
    }

    @Override
    public Reference create(Reference reference) {
        return referenceRepo.save(reference);
    }

    @Override
    public Reference update(String id, Reference reference) {
        Reference ref = referenceRepo.findById(id);
        ref.setTitle(reference.getTitle());
        return ref;
    }

    @Override
    public boolean delete(String id) {
        Reference reference = referenceRepo.findById(id);
        if (reference != null) {
            referenceRepo.delete(reference);
            return true;
        }
        return false;
    }
}
