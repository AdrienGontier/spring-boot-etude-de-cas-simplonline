package com.simplonline.EDCsimplonline.services.servicesImpl;

import com.simplonline.EDCsimplonline.entities.Attachment;
import com.simplonline.EDCsimplonline.entities.Link;
import com.simplonline.EDCsimplonline.entities.Render;
import com.simplonline.EDCsimplonline.enums.RenderType;
import com.simplonline.EDCsimplonline.exceptions.ResourceNotFoundException;
import com.simplonline.EDCsimplonline.repositories.RenderRepository;
import com.simplonline.EDCsimplonline.services.AttachmentService;
import com.simplonline.EDCsimplonline.services.LinkService;
import com.simplonline.EDCsimplonline.services.RenderService;
import com.simplonline.EDCsimplonline.services.UploadFileService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class RenderServiceImpl implements RenderService {
    private final RenderRepository renderRepository;
    private final UploadFileService uploadFileService;
    private final AttachmentService attachmentService;
    private final LinkService linkService;

    public RenderServiceImpl(RenderRepository renderRepository, UploadFileService uploadFileService, AttachmentService attachmentService, LinkService linkService) {
        this.renderRepository = renderRepository;
        this.uploadFileService = uploadFileService;
        this.attachmentService = attachmentService;
        this.linkService = linkService;
    }

    @Override
    public Render createRender(Render renderRequest, MultipartFile[] files) {

        renderRequest.setType(RenderType.INDIVIDUAL);
        var renderSaved = renderRepository.save(renderRequest);
        List<Attachment> attachmentList= new ArrayList<>();
        List<Link> linkList = new ArrayList<>();
        for(var l: renderSaved.getLinks()) {
            var link = new Link();
            link.setRender(getRenderById(renderSaved.getRenderId()));
            link.setUrl(l.getUrl());
            var linkSaved = linkService.createLink(link);
            linkList.add(linkSaved);
        }

        for(var file: files) {
            var attachment = new Attachment();
            attachment.setUrl(uploadFileService.storeFile( file ));
            attachment.setRender(renderSaved);
            attachmentList.add(attachmentService.createAttachment(attachment));
        }

        renderSaved.setLinks(linkList);
        renderSaved.setAttachments(attachmentList);

        return renderSaved;
    }

    @Override
    public Render getRenderById(String renderId) {
        return renderRepository.findById(renderId).orElseThrow(()->
            new ResourceNotFoundException("Ce Rendue n'existe pas.")
        );
    }

    @Override
    public Render updateRender(String renderId, Render renderRequest) {
        System.out.println(renderRequest.getBrief().getBriefId());
        var render = getRenderById(renderId);
        render.setMessage(renderRequest.getMessage());
        renderRequest.setType(RenderType.INDIVIDUAL);
        return renderRepository.save(render);
    }

    @Override
    public String deleteRender(String renderId) throws URISyntaxException {
        var render = getRenderById(renderId);

        for(var attachment: render.getAttachments()) {
            String filename = Paths.get(new URI(attachment.getUrl()).getPath()).getFileName().toString();
            uploadFileService.deleteFile(filename);
        }
        for(var link: render.getLinks()) {
            linkService.deleteLink(link.getLinkId());
        }

        renderRepository.delete(render);
        return "Le rendue a bien été supprimé.";
    }

    @Override
    public Page<Render> getAllRenders(int page, int size) {
        return renderRepository.findAll(PageRequest.of(page, size));
    }
}
