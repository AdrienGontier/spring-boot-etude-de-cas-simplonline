package com.simplonline.EDCsimplonline.services.servicesImpl;

import com.simplonline.EDCsimplonline.entities.Resource;
import com.simplonline.EDCsimplonline.entities.Resource;
import com.simplonline.EDCsimplonline.repositories.ResourceRepo;
import com.simplonline.EDCsimplonline.services.ResourceService;
import com.simplonline.EDCsimplonline.services.ResourceService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class ResourceServiceImpl implements ResourceService {

    private final ResourceRepo resourceRepo;

    public ResourceServiceImpl(ResourceRepo resourceRepo) {
        this.resourceRepo = resourceRepo;
    }
    @Override
    public Collection<Resource> getAll() {
        return resourceRepo.findAll();
    }

    @Override
    public Resource findById(String id) {
        return resourceRepo.findById(id);
    }

    @Override
    public Resource create(Resource resource) {
        return resourceRepo.save(resource);
    }

    @Override
    public Resource update(String id, Resource resource) {
        Resource ref = resourceRepo.findById(id);

        if (resource.getDescription() != null) {
            ref.setDescription(resource.getDescription());
        }

        if (resource.getUrl() != null) {
            ref.setUrl(resource.getUrl());
        }

        if (resource.getTitle() != null) {
            ref.setTitle(resource.getTitle());
        }

        return ref;
    }

    @Override
    public boolean delete(String id) {
        Resource Resource = resourceRepo.findById(id);
        if (Resource != null) {
            resourceRepo.delete(Resource);
            return true;
        }
        return false;
    }
}
