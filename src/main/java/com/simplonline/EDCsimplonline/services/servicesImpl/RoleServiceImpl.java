package com.simplonline.EDCsimplonline.services.servicesImpl;

import com.simplonline.EDCsimplonline.entities.Role;
import com.simplonline.EDCsimplonline.exceptions.ResourceNotFoundException;
import com.simplonline.EDCsimplonline.repositories.RoleRepository;
import com.simplonline.EDCsimplonline.services.RoleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role createRole(Role roleRequest) {
        return roleRepository.save(roleRequest);
    }

    @Override
    public Role getRoleById(String roleId) {
        return roleRepository.findById(roleId).orElseThrow(()->
            new ResourceNotFoundException("Ce rôle n'existe pas.")
        );
    }

    @Override
    public Role updateRole(String roleId, Role roleRequest) {
        var role = getRoleById(roleId);
        role.setName(roleRequest.getName());
        return roleRepository.save(role);
    }

    @Override
    public String deleteRole(String roleId) {
        var role = getRoleById(roleId);
        roleRepository.delete(role);
        return "Le rôle a bien été supprimé";
    }

    @Override
    public Page<Role> getAllRoles(int page, int size) {
        return roleRepository.findAll(PageRequest.of(page, size));
    }
}
