package com.simplonline.EDCsimplonline.services.servicesImpl;

import com.simplonline.EDCsimplonline.entities.Skill;
import com.simplonline.EDCsimplonline.exceptions.ResourceNotFoundException;
import com.simplonline.EDCsimplonline.repositories.SkillRepository;
import com.simplonline.EDCsimplonline.services.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class SkillServiceImpl implements SkillService {

    private final SkillRepository skillRepository;

    @Autowired
    public SkillServiceImpl(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @Override
    public Page<Skill> getAllSkills(int page, int size) {
        return skillRepository.findAll(PageRequest.of(page, size));
    }

    @Override
    public Skill getSkillById(String skillId) {
        return skillRepository.findById(skillId).orElseThrow(()->
                new ResourceNotFoundException("Cette competence n'existe pas")
        );
    }

    @Override
    public Skill createSkill(Skill skillRequest) {
        return skillRepository.save(skillRequest);
    }

    @Override
    public Skill updateSkill(String skillId, Skill skillRequest) {
        var skill = getSkillById(skillId);
        skill.setTitle(skillRequest.getTitle());
        skill.setLevel(skillRequest.getLevel());
        return skillRepository.save(skill);
    }

    @Override
    public String deleteSkill(String skillId) {
        var skill = getSkillById(skillId);
        skillRepository.delete(skill);
        return "La competence a bien été supprimé";
    }
}
