package com.simplonline.EDCsimplonline.services.servicesImpl;


import com.simplonline.EDCsimplonline.entities.Brief;
import com.simplonline.EDCsimplonline.exceptions.ResourceNotFoundException;
import com.simplonline.EDCsimplonline.repositories.BriefRepository;
import com.simplonline.EDCsimplonline.services.BriefService;
import com.simplonline.EDCsimplonline.services.RenderService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.util.Date;

@Service
public class BriefServiceImpl implements BriefService {

    private final BriefRepository briefRepository;

    private final RenderService renderService;

    public BriefServiceImpl(BriefRepository briefRepository, RenderService renderService) {
        this.briefRepository = briefRepository;
        this.renderService = renderService;
    }

    @Override
    public Brief createBrief(Brief briefRequest) {
        briefRequest.setDate(new Date());
        return briefRepository.save(briefRequest);
    }

    @Override
    public Brief getBriefById(String briefId) {
        return briefRepository.findById(briefId).orElseThrow(()->
                new ResourceNotFoundException("Ce brief n'existe pas.")
        );
    }

    @Override
    public Brief updateBrief(String briefId, Brief briefRequest) {
        var brief = getBriefById(briefId);
        brief.setTitle(briefRequest.getTitle());
        brief.setDescription(briefRequest.getDescription());
        brief.setContent(briefRequest.getContent());
        brief.setDeliverable(briefRequest.getDeliverable());
        brief.setNotification(briefRequest.getNotification());
        brief.setPedagogicalModality(briefRequest.getPedagogicalModality());
        return briefRepository.save(brief);
    }

    @Override
    public String deleteBrief(String briefId) throws URISyntaxException {
        var brief = getBriefById(briefId);
        renderService.deleteRender(brief.getRender().getRenderId());
        briefRepository.delete(brief);
        return "Le brief a bien été supprimé";
    }

    @Override
    public Page<Brief> getAllBriefs(int page, int size) {
        return briefRepository.findAll(PageRequest.of(page, size));
    }
}
