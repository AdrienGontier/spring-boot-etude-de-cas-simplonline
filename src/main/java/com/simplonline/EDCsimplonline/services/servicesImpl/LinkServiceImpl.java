package com.simplonline.EDCsimplonline.services.servicesImpl;

import com.simplonline.EDCsimplonline.entities.Link;
import com.simplonline.EDCsimplonline.exceptions.ResourceNotFoundException;
import com.simplonline.EDCsimplonline.repositories.LinkRepository;
import com.simplonline.EDCsimplonline.services.LinkService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LinkServiceImpl implements LinkService {
    private final LinkRepository linkRepository;

    public LinkServiceImpl(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    @Override
    public Link createLink(Link linkRequest) {
        return linkRepository.save(linkRequest);
    }

    @Override
    public Link getLinkById(String linkId) {
        return linkRepository.findById(linkId).orElseThrow(()->
            new ResourceNotFoundException("Ce lien n'existe pas.")
        );
    }

    @Override
    public Link updateLink(String linkId, Link linkRequest) {
        var link = getLinkById(linkId);
        link.setUrl(linkRequest.getUrl());
        return linkRepository.save(link);
    }

    @Override
    public String deleteLink(String linkId) {
        var link = getLinkById(linkId);
        linkRepository.delete(link);
        return "Le lien a bien été supprimé.";
    }

    @Override
    public Page<Link> getAllLinks(int page, int size) {
        return linkRepository.findAll(PageRequest.of(page, size));
    }
}
