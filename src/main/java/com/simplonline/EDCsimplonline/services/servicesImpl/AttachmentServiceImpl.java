package com.simplonline.EDCsimplonline.services.servicesImpl;

import com.simplonline.EDCsimplonline.entities.Attachment;
import com.simplonline.EDCsimplonline.exceptions.ResourceNotFoundException;
import com.simplonline.EDCsimplonline.repositories.AttachmentRepository;
import com.simplonline.EDCsimplonline.services.AttachmentService;
import com.simplonline.EDCsimplonline.services.UploadFileService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AttachmentServiceImpl implements AttachmentService {
    private final AttachmentRepository attachmentRepository;
    private final UploadFileService uploadFileService;

    public AttachmentServiceImpl(AttachmentRepository attachmentRepository, UploadFileService uploadFileService) {
        this.attachmentRepository = attachmentRepository;
        this.uploadFileService = uploadFileService;
    }

    @Override
    public Attachment createAttachment(Attachment attachmentRequest) {
        return attachmentRepository.save(attachmentRequest);
    }

    @Override
    public Attachment getAttachmentById(String attachmentId) {
        return attachmentRepository.findById(attachmentId).orElseThrow(()->
            new ResourceNotFoundException("Cette pièce joint n'existe pas.")
        );
    }

    @Override
    public void deleteAttachment(String attachmentId) {
        var attachment = getAttachmentById(attachmentId);
        attachmentRepository.delete(attachment);
    }

    @Override
    public Page<Attachment> getAllAttachments(int page, int size) {
        return attachmentRepository.findAll(PageRequest.of(page, size));
    }
}
