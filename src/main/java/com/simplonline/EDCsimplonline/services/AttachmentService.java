package com.simplonline.EDCsimplonline.services;

import com.simplonline.EDCsimplonline.entities.Attachment;
import org.springframework.data.domain.Page;

public interface AttachmentService {
    Attachment createAttachment(Attachment attachmentRequest);

    Attachment getAttachmentById(String attachmentId);

    void deleteAttachment(String attachmentId);

    Page<Attachment> getAllAttachments(int page, int size);
}
