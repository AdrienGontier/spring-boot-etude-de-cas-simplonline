package com.simplonline.EDCsimplonline.services;

import com.simplonline.EDCsimplonline.entities.User;
import org.springframework.data.domain.Page;

public interface UserService {

    Page<User> getAllUsers(int page, int size);

    User getUserById(String userId);

    User createUser(User userRequest);

    User updateUser(String userId, User userRequest);

    String deleteUser(String userId);
}
