package com.simplonline.EDCsimplonline.services;

import com.simplonline.EDCsimplonline.entities.Role;
import org.springframework.data.domain.Page;

public interface RoleService {
    Role createRole(Role roleRequest);

    Role getRoleById(String roleId);

    Role updateRole(String roleId, Role roleRequest);

    String deleteRole(String roleId);

    Page<Role> getAllRoles(int page, int size);
}
