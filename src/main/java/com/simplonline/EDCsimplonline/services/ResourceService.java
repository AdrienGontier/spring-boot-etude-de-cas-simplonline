package com.simplonline.EDCsimplonline.services;

import com.simplonline.EDCsimplonline.entities.Resource;

import java.util.Collection;

public interface ResourceService {

    Collection<Resource> getAll();

    Resource findById(String id);

    Resource create(Resource Resource);

    Resource update(String id, Resource Resource);

    boolean delete(String id);
}
