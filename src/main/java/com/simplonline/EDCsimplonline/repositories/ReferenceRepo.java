package com.simplonline.EDCsimplonline.repositories;

import com.simplonline.EDCsimplonline.entities.Reference;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReferenceRepo extends JpaRepository<Reference, Long> {
    Reference findById(String id);

}
