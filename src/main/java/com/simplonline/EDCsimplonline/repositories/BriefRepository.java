package com.simplonline.EDCsimplonline.repositories;


import com.simplonline.EDCsimplonline.entities.Brief;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BriefRepository extends JpaRepository<Brief, String> {
}
