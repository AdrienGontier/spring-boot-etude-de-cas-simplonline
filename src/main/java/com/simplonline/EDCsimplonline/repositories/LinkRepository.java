package com.simplonline.EDCsimplonline.repositories;

import com.simplonline.EDCsimplonline.entities.Link;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LinkRepository extends JpaRepository<Link, String> {
}
