package com.simplonline.EDCsimplonline.repositories;

import com.simplonline.EDCsimplonline.entities.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SkillRepository extends JpaRepository<Skill, String> {
}
