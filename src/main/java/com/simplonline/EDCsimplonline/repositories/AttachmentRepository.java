package com.simplonline.EDCsimplonline.repositories;

import com.simplonline.EDCsimplonline.entities.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentRepository extends JpaRepository<Attachment, String> {
}
