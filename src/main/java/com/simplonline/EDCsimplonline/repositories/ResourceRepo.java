package com.simplonline.EDCsimplonline.repositories;

import com.simplonline.EDCsimplonline.entities.Resource;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResourceRepo extends JpaRepository<Resource, Long> {
    Resource findById(String id);
}
