package com.simplonline.EDCsimplonline.repositories;

import com.simplonline.EDCsimplonline.entities.Render;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RenderRepository extends JpaRepository<Render, String> {
}
