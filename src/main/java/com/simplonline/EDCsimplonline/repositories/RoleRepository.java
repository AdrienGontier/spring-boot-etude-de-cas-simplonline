package com.simplonline.EDCsimplonline.repositories;

import com.simplonline.EDCsimplonline.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, String> {
}
