package com.simplonline.EDCsimplonline.repositories;

import com.simplonline.EDCsimplonline.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
